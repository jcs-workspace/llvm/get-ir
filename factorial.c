/**
 * $File: factorial.c $
 * $Date: 2023-12-12 16:34:03 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2023 by Shen, Jen-Chieh $
 */

#include <stdio.h>

int factorial(int n) {
  int fact = 1;
  for (int x = 1; x <= n; ++x)
    fact = fact * x;
  return fact;
}

int main(int argc, char *argv[]) {
  printf("Hello World!");
  printf("Number: %d", factorial(10));
  return 0;
}
